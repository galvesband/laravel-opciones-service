<?php

use Illuminate\Support\Facades\Route;
use Beat\PaqueteOpcionesBeat\Http\Controllers\OpcionesController;

Route::controller(OpcionesController::class)->group(function () {

    Route::get('/opciones/claves', 'listarClaves');
    Route::get('/opciones', 'obtenerOpciones');
    Route::get('/opciones/{clave_opcion}', 'obtenerOpcion');
    Route::get('/opciones/modelos', 'obtenerModelos');
    Route::put('/opciones', 'actualizarOpciones');
    Route::put('/opciones/{clave_opcion}', 'actualizarOpcion');
});
