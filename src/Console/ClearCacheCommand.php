<?php

namespace Beat\PaqueteOpcionesBeat\Console;

use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class ClearCacheCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'beat-opciones:cache-clear {opciones?*} {--id_modelo=} {--clase_modelo=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Borra la cache de una o varias claves de opciones. Si no se proporcionan claves, se borrarán todas';

    public function handle()
    {
        if (! $this->confirmToProceed("BEAT-OPCIONES: Se borrarán de la caché todas las opciones! Está segur@?", $this->shouldConfirm())) {
            return self::FAILURE;
        }

        $id_modelo    = $this->option('id_modelo');
        $clase_modelo = $this->option('clase_modelo');
        
        /** @var Model|null */
        $modelo       = null;

        // Si introducen una de los dos, deben introducir las dos
        if((!is_null($id_modelo) && is_null($clase_modelo)) || (is_null($id_modelo) && !is_null($clase_modelo))) {
            $this->error("Datos incompleto del modelo. Proporcione ID y clase.");
            return self::FAILURE;
        }

        if($id_modelo && $clase_modelo) {

            // Modelo resuelto para obtener las opciones con relación a él
            try {
                $modelo = $clase_modelo::find($id_modelo);
            } catch (\Throwable $th) {
                $this->error("Error al intentar resolver el modelo {$clase_modelo}#{$id_modelo}");
                return self::FAILURE;
            }
        }

        $claves_opciones = blank($this->argument('opciones')) ? array_keys(OpcionFacade::opciones()) : $this->argument('opciones');

        if(!is_array($claves_opciones)) {
            $claves_opciones = [$claves_opciones];
        }

        foreach ($claves_opciones as $clave) {   
            if(OpcionFacade::deleteFromCache($clave, $modelo)) {
                $this->info("Se ha eliminado la cache de la opción '{$clave}'.");
            }
        }

        return self::SUCCESS;

    }

    private function shouldConfirm()
    {
        return blank($this->argument('opciones'));
    }
}
