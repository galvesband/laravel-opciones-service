<?php

namespace Beat\PaqueteOpcionesBeat\Services;

use Beat\PaqueteOpcionesBeat\Contracts\IBeatOpcionesService;
use Beat\PaqueteOpcionesBeat\Models\Opcion;
use Beat\PaqueteOpcionesBeat\Exceptions\OpcionDesconocidaException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use RuntimeException;
use LogicException;
use Illuminate\Support\Facades\DB;

class OpcionesService implements IBeatOpcionesService
{

    const SEGUNDOS_DIA = 60 * 60 * 24;

    const SEGUNDOS_AÑO = self::SEGUNDOS_DIA * 365;

    public static function logChannel() : string
    {
        return config('beat_opciones.log_channel', 'main');
    }

    public function resolverKeyCache($clave, $modelo = null) : string
    {
        $key = $clave;

        if ($modelo) {
            $clase_modelo = get_class($modelo);
            $key .= "_{$modelo->id}_{$clase_modelo}";
        }

        return $key;
    }

    public function deleteFromCache($clave, $modelo = null)
    {
        return Cache::tags(config('beat_opciones.cache_tags'))->forget($this->resolverKeyCache($clave, $modelo));
    }

    public function getModelo($clave, $modelo = null) : Opcion
    {
        // Intentamos obtener su valor de la tabla de opciones, y si no, lo creamos con el valor por defecto
        return Opcion::query()
            ->where('clave', $clave)
            ->where('opcionable_id', $modelo ? $modelo->id : null)
            ->where('opcionable_type', $modelo ? get_class($modelo) : null)
            ->firstOr(function () use ($clave, $modelo) {
                $configuracion = $this->getConfiguracionOpcion($clave);

                $valor = $configuracion['default'];

                /** @noinspection PhpUndefinedMethodInspection */
                return $this->guardarOpcion($clave, $valor, $modelo);
            });
    }

    public function get(string $nombreOpcion, $modelo = null) : ?string
    {
        // Intentamos construir la opción a partir de caché
        $valor = Cache::tags(config('beat_opciones.cache_tags'))->get($this->resolverKeyCache($nombreOpcion, $modelo), config('beat_opciones.valor_cache_por_defecto'));

        // Si el valor no estaba cacheado...
        if ($valor === config('beat_opciones.valor_cache_por_defecto')) {
            /** @var Opcion $opcion */
            $opcion = $this->getModelo($nombreOpcion, $modelo);

            $valor = $opcion->valor;
        }

        return is_array($valor) ? json_encode($valor) : $valor;
    }

    /**
     * @param string     $nombreOpcion
     * @param Model|null $modelo
     * @return array|float|int|mixed|string|bool
     * @throws OpcionDesconocidaException
     */
    public function getTyped(string $nombreOpcion, $modelo = null)
    {
        $configuracion = $this->getConfiguracionOpcion($nombreOpcion);
        $valor         = $this->get($nombreOpcion, $modelo);

        switch ($configuracion['tipo']) {
            case 'bool':
                $valor = boolval($valor);
            case 'string':
                break;

            case 'integer':
                $valor = intval($valor);
                break;

            case 'float':
                $valor = floatval($valor);
                break;

            case 'array':
            case 'json':
                $valor = json_decode($valor, true);
                break;

            default:
                throw new LogicException("Tipo de opción desconocido: '{$configuracion['tipo']}'.");
        }

        return $valor;
    }

    public function set(string $nombreOpcion, $valor, ?Model $modelo = null)
    {

        return DB::transaction(function () use ($nombreOpcion, $valor, $modelo) {
            /** @var Opcion $opcion */
            $opcion = $this->guardarOpcion($nombreOpcion, $valor, $modelo);

            if (! $opcion || $opcion->isDirty()) {
                throw new RuntimeException("Error de BD al guardar Opcion#$opcion->id " .
                    "('$opcion->clave': '$opcion->valor').");
            }

            Log::channel(self::logChannel())
                ->info("Opcion actualizada.", ['clave' => $nombreOpcion, 'valor' => $valor]);

                return $valor;
        }, 2);

    }

    /**
     * Devuelve la configuración de la opción. Si la opción es desconocida lanza un InvalidArgumentException.
     *
     * @param string $nombreOpcion
     * @return array
     * @throws OpcionDesconocidaException
     */
    private function getConfiguracionOpcion(string $nombreOpcion) : array
    {
        if (! key_exists($nombreOpcion, static::opciones())) {
            throw new OpcionDesconocidaException($nombreOpcion);
        }

        return static::opciones()[$nombreOpcion];
    }

    public static function opciones() : array
    {
        return config('beat_opciones.opciones') ?? [];
    }

    /**
     * Método que guarda en BD el valor de una opción.
     * El valor debe venir previamente validado
     * @param string     $nombreOpcion
     * @param mixed      $valor
     * @param Model|null $modelo
     * @return Opcion
     */
    private function guardarOpcion($nombreOpcion, $valor, $modelo = null)
    {

        $configuracion = $this->getConfiguracionOpcion($nombreOpcion);

        // Validamos el nuevo valor
        $valor = $this->validate($configuracion, $valor);

        // Comprobar que el modelo que vamos a relacionar es compatible
        if ($modelo) {
            $clase_modelo = get_class($modelo);

            throw_if(
                ! key_exists('relaciones', $configuracion) ||
                ! in_array(get_class($modelo), $configuracion['relaciones']),
                new LogicException("El modelo proporcionado '{$clase_modelo}' no es compatible con la opción '{$nombreOpcion}'.")
            );
        }

        if (is_array($valor)) {
            $valor = json_encode($valor);
        }


        $opcion = Opcion::updateOrCreate(
            [
                'clave'           => $nombreOpcion,
                'opcionable_id'   => optional($modelo)->id,
                'opcionable_type' => $clase_modelo ?? null
            ],
            [
                'valor' => $valor
            ]
        );

        $configuracion = $this->getConfiguracionOpcion($nombreOpcion);

        // Insertamos en caché
        Cache::tags(config('beat_opciones.cache_tags'))->set($this->resolverKeyCache($nombreOpcion, $modelo), $opcion->valor, $configuracion['cache'] ?? null);

        return $opcion;
    }

    /**
     * Si la configuración a modificar/crear tiene definida unas reglas
     * de validación, se le aplicarán.
     * Si no pasa la validación, se lanzará un error ValidationException.
     * @param array $configuracion 
     * Configuración a modificar/crear
     * @param mixed $valor
     * Valor a validar
     * @return mixed Devuelve el valor dado después de ser validado
     * @throws ValidationException
     */
    private function validate($configuracion, $valor)
    {

        // Comprobamos 
        if (! key_exists('rules', $configuracion))
            return $valor;

        $validacion = Validator::make(['data' => $valor], $configuracion['rules'], $configuracion['messages'] ?? []);

        if ($validacion->fails()) {
            throw new ValidationException($validacion);
        }

        return $valor;
    }

    /**
     * Comprueba si una opción es modificable o no
     * @param string $clave
     * @return bool
     * @throws LogicException
     */
    public function esModificable($clave)
    {
        $configuracionOpcion = $this->getConfiguracionOpcion($clave);

        throw_if(
            key_exists('modificable', $configuracionOpcion) &&
            ! $configuracionOpcion['modificable'] &&
            ! app()->runningInConsole(),
            new LogicException("La opción no es modificable.", 403)
        );

        return true;
    }

    /**
     * Obtiene si a una opción se le aplica política
     * @param string $clave
     * @return array
     */
    public function getPolicy($clave)
    {
        $configuracionOpcion = $this->getConfiguracionOpcion($clave);

        return $configuracionOpcion['policy'] ?? [];
    }

}
