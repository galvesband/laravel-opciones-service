<?php
namespace Beat\PaqueteOpcionesBeat;

use Beat\PaqueteOpcionesBeat\Console\ClearCacheCommand;
use Beat\PaqueteOpcionesBeat\Providers\AuthServiceProvider;
use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Foundation\CachesRoutes;

class BeatOpcionesServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        parent::register();

        $this->app->register(AuthServiceProvider::class);

        // Registramos la fachada de opciones
        $this->app->singleton('opcion', function () {
            return new OpcionesService;
        });

        $this->configure();
    }

    public function boot()
    {
        $this->publishFiles();
        $this->registerRoutes();
        $this->registerMigrations();
        $this->registerCommands();
        
    }

    /**
     * Establecemos las configuraciones
     * @return void
     */
    protected function configure()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/beat_opciones.php',
            'beat_opciones'
        );
    }

    /**
     * Registramos las migraciones
     */
    protected function registerMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . "/../database/migrations");
    }

    /**
     * Registramos las rutas con las configuraciones
     */
    protected function registerRoutes()
    {
        if ($this->app instanceof CachesRoutes && $this->app->routesAreCached()) {
            return;
        }
        
        // Patrón para validar las claves de opciones en las rutas
        Route::pattern('clave_opcion', '[A-Z_]+');

        Route::group([
            'prefix' => config('beat_opciones.path', ''),
            'middleware' => config('beat_opciones.middlewares', 'web')
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        });

    }

    protected function registerCommands()
    {
        if($this->app->runningInConsole()) {
            $this->commands([
                ClearCacheCommand::class
            ]);
        }
    }

    /**
     * Registra en Laravel los archivos publicables por el paquete.
     *
     * @return void
     */
    protected function publishFiles(): void
    {
        if ($this->app->runningInConsole()) {

            // Configuraciones
            $this->publishes([
                __DIR__ . '/../config/beat_opciones.php' => config_path('beat_opciones.php'),
            ], 'config');

            // Migraciones
            $this->publishes([
                __DIR__ . '/../database/migrations/' =>
                    $this->app->databasePath('./migrations/')
            ], 'migrations');

            // Service provider
            $this->publishes([
                __DIR__ . '/BeatOpcionesServiceProvider.php' => app_path('Providers/BeatOpcionesServiceProvider.php')
            ], 'providers');
        }
    }

}