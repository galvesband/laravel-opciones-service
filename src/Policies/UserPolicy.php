<?php

namespace Beat\PaqueteOpcionesBeat\Policies;

use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Política SÓLO para test
 */
class UserPolicy
{
    use HandlesAuthorization;

    // Sólo tienen permiso los usuarios llamados PAPA
    public function updateOpcion(FakeUser $user)
    {
        if($user->name == 'PAPA') {
            return $this->allow();
        }

        return $this->deny();
    }
}
