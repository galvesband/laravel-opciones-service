<?php

namespace Beat\PaqueteOpcionesBeat\Events;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Beat\PaqueteOpcionesBeat\Models\Opcion;

class OpcionModificadaEvent 
{
    use Dispatchable, SerializesModels;
    
    public Opcion $opcion;

    /**
     * @return void
     */
    public function __construct(Opcion $opcion)
    {
        $this->opcion = $opcion;
    }
}
