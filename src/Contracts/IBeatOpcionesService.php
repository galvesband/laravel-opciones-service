<?php
namespace Beat\PaqueteOpcionesBeat\Contracts;

use Beat\PaqueteOpcionesBeat\Models\Opcion;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Illuminate\Validation\ValidationException;

interface IBeatOpcionesService 
{

    /**
     * Obtiene el valor de una Opción.
     *
     * Si la Opción está cacheada utilizará ese valor.
     *
     * Si no está cacheada buscará el valor de la Opción en BD. De no existir la creará, 
     * usando su valor por defecto e insertándola en caché.
     *
     * @param string     $nombreOpcion
     * @param Model|null $modelo
     * @return ?string
     * @throws InvalidArgumentException
     */
    public function get(string $nombreOpcion, ?Model $modelo = null): ?string;

    /**
     * Establece el valor de una Opción en BD y caché.
     * @param  string        $nombreOpcion Nombre de la Opción
     * @param  mixed         $valor        Valor de la Opción que se quiere establecer.
     * @param Model|null     $modelo
     * @return mixed
     * @throws InvalidArgumentException
     * @throws ValidationException
     */
    public function set(string $nombreOpcion, $valor, ?Model $modelo = null);

     /**
     * Obtiene el valor de una Opción, como 'get', pero con conversión de tipo del valor retornado.
     *
     * @param string         $nombreOpcion
     * @param Model|null     $modelo
     * @return string|int|float|array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getTyped(string $nombreOpcion, $modelo = null);

    /**
     * Opciones disponibles
     */
    public static function opciones(): array;

    /**
     * Devuelve la key que debe usar esa opción para ese modelo en cache
     * @param string     $clave
     * @param Model|null $modelo
     * @return string
     */
    public function resolverKeyCache($clave, $modelo):string;

    /**
     * Elimina el valor de la opción en caché
     * @param string     $clave
     * @param Model|null $modelo
     * @return bool
     */
    public function deleteFromCache($clave, $modelo = null);

    /**
     * Devuelve el registro en base de datos de la opción
     * @param string     $clave
     * @param Model|null $modelo
     */
    public function getModelo($clave, $modelo): Opcion;
}