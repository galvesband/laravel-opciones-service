<?php

namespace Beat\PaqueteOpcionesBeat\Facades;

use Illuminate\Support\Facades\Facade;


class OpcionFacade extends Facade 
{
    protected static function getFacadeAccessor()
    {
        return 'opcion';
    }
}
