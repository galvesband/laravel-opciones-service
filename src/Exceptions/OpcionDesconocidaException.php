<?php

namespace Beat\PaqueteOpcionesBeat\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Throwable;

class OpcionDesconocidaException extends Exception
{
    private string $clave_desconocida;

    public function __construct(string $clave_desconocida = "", string $message = "", int $code = 404, Throwable $previous = null)
    {
        
        $this->clave_desconocida = $clave_desconocida;

        if(blank($message)){
            $message = "Opción desconocida. Clave '{$this->clave_desconocida}' no encontrada.";
        }

        parent::__construct($message, $code, $previous);
    }

    public function render(): JsonResponse
    {
        return response()->json([
            'message' => "Opción desconocida. Calve '{$this->clave_desconocida}' no encontrada."
        ], 404);
    }

    public function getClaveDesconocida(): string
    {
        return $this->clave_desconocida;
    }
}
