<?php

namespace Beat\PaqueteOpcionesBeat\Providers;

use Beat\PaqueteOpcionesBeat\Policies\UserPolicy;
use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        FakeUser::class => UserPolicy::class
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
