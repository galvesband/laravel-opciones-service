<?php

namespace Beat\PaqueteOpcionesBeat\Http\Requests;

use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActualizarOpcionesRequest extends FormRequest
{
    public function rules()
    {
        return [
            "opciones"                  => "required|array",
            "opciones.*"                => "required|array",
            "opciones.*.clave"          => ["required",'string', Rule::in(array_keys(OpcionFacade::OPCIONES()))],
            "opciones.*.valor"          => ["required"],
            "opciones.*.modelo_clase"   => ["sometimes", 'required_with:modelo_id', 'string'],
            "opciones.*.modelo_id"      => ["sometimes", 'required_with:modelo_clase', 'numeric'],
        ];
    }

    /**
     * @return array devuelve un array clave/valor con las opciones actualizadas
     */
    public function actualizarOpciones(): Collection
    {
        $opciones_resueltas = collect();

        foreach ($this->input('opciones') as $opcion) {

            // Resolver modelo
            if(key_exists('modelo_id', $opcion)){
                $clase = $opcion['modelo_clase'];
                $id    = $opcion['modelo_id'];
    
                $modelo_resuelto = $clase::find($id);
            }
            
            if(OpcionFacade::esModificable($opcion['clave'])) {
                   
                // Actualizamos la opción
                OpcionFacade::set($opcion['clave'], $opcion['valor'], $modelo_resuelto ?? null);
                
                $opciones_resueltas->add(OpcionFacade::getModelo($opcion['clave'], $modelo_resuelto ?? null));
            }
        }

        return $opciones_resueltas;
    }
}
