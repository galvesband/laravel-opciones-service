<?php

namespace Beat\PaqueteOpcionesBeat\Http\Requests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

class ShowOpcionRequest extends FormRequest
{

    public function rules()
    {
        return [
            'modelo_clase'     => ['sometimes', 'required_with:modelo_clase', 'string'],
            'modelo_id'        => ['sometimes', 'required_with:modelo_id', 'numeric'],
        ];
    }

    /**
     * @return Model|null devuelve un modelo resuelto
     */
    public function resolverModelo(): ?Model
    {
        if($this->has('modelo_id')){
            $clase = $this->input('modelo_clase');
            $id    = $this->input('modelo_id');

            $modelo_resuelto = $clase::find($id);
        }

        return $modelo_resuelto ?? null;
    }
}
