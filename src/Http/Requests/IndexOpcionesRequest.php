<?php

namespace Beat\PaqueteOpcionesBeat\Http\Requests;

use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;

class IndexOpcionesRequest extends FormRequest
{

    public function rules()
    {
        return [
            'modelo_clase'     => ['sometimes', 'required_with:modelo_clase', 'string'],
            'modelo_id'        => ['sometimes', 'required_with:modelo_id', 'numeric'],
            "opciones"         => "required|array",
            "opciones.*"       => ["required", Rule::in(array_keys(OpcionFacade::opciones()))],
        ];
    }

    /**
     * @return array devuelve un array clave/valor con las opciones pedidas
     */
    public function obtenerOpciones(): Collection
    {
        $opciones_resueltas = collect();

        if($this->has('modelo_id')){
            $clase = $this->input('modelo_clase');
            $id    = $this->input('modelo_id');

            $modelo_resuelto = $clase::find($id);
        }

        foreach ($this->input('opciones') as $clave_opcion) {
            $opciones_resueltas->add(OpcionFacade::getModelo($clave_opcion, $modelo_resuelto ?? null));
        }

        return $opciones_resueltas;
    }
}
