<?php

namespace Beat\PaqueteOpcionesBeat\Http\Requests;

use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class ActualizarOpcionRequest extends FormRequest
{

    public function authorize()
    {
        $clave_opcion = $this->route('clave_opcion');

        // Si en la ruta no encontramos la clave, no podemos continuar
        if (! $clave_opcion || ! is_string($clave_opcion)) {
            Log::channel(OpcionFacade::logChannel())
                ->info("Clave de opción no encontrada en ruta o mal formada, abortando modificación.", ['clave' => $clave_opcion]);

            return false;
        }

        $policiy = OpcionFacade::getPolicy($clave_opcion);

        // Si no hay política para esa opción, pasamos de largo
        if (blank($policiy)) {
            return true;
        }

        /** @var \Illuminate\Foundation\Auth\Access\Authorizable|null */
        $user = $this->user();

        // Si hay política y no hay usuario, no podemos continuar
        if (! $user) {
            Log::channel(OpcionFacade::logChannel())
                ->info("Usuario no identificado, abortando modificación.", ['clave' => $clave_opcion]);
            return false;
        }

        Log::channel(OpcionFacade::logChannel())
            ->info("Aplicando política para actualizar opción.", [
                'clave'           => $clave_opcion,
                'clase_politica'  => $policiy['clase'],
                'metodo_politica' => $policiy['metodo']
            ]);

        // Aplicamos la política
        return $user->can($policiy['metodo'] ?? '', $policiy['clase'] ?? '');

    }

    public function rules()
    {
        return [
            "valor"        => ["required"],
            "modelo_clase" => ["sometimes", 'required_with:modelo_id', 'string'],
            "modelo_id"    => ["sometimes", 'required_with:modelo_clase', 'numeric'],
        ];
    }

    /**
     * @return Model|null devuelve un modelo resuelto
     */
    public function resolverModelo() : ?Model
    {
        if ($this->has('modelo_id')) {
            $clase = $this->input('modelo_clase');
            $id    = $this->input('modelo_id');

            $modelo_resuelto = $clase::find($id);
        }

        return $modelo_resuelto ?? null;
    }
}
