<?php
namespace Beat\PaqueteOpcionesBeat\Http\Controllers;

use Beat\PaqueteOpcionesBeat\Exceptions\OpcionDesconocidaException;
use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Beat\PaqueteOpcionesBeat\Http\Requests\ActualizarOpcionesRequest;
use Beat\PaqueteOpcionesBeat\Http\Requests\ActualizarOpcionRequest;
use Beat\PaqueteOpcionesBeat\Http\Requests\IndexOpcionesRequest;
use Beat\PaqueteOpcionesBeat\Http\Requests\ShowOpcionRequest;
use Beat\PaqueteOpcionesBeat\Http\Resources\OpcionResource;


class OpcionesController extends Controller
{
    public function listarClaves()
    {
        $data = array_map(
            fn($opcion) => ['reglas' => $opcion['rules'], 'modificable' => $opcion['modificable'] ?? true],
            OpcionFacade::opciones()
        );

        return response()->json(['data' => $data]);
    }

    public function obtenerOpciones(IndexOpcionesRequest $request)
    {

        return response()->json(["data" => OpcionResource::collection($request->obtenerOpciones())]);
    }

    public function obtenerOpcion(string $clave_opcion, ShowOpcionRequest $request)
    {
    
        try {
            return response()->json(['data' => OpcionResource::make(OpcionFacade::getModelo($clave_opcion, $request->resolverModelo()))]);
        } catch (OpcionDesconocidaException $e) {
            return response("Opción desconocida: '{$e->getClaveDesconocida()}'", 404);
        }
    }

    /**
     * Actualiza todas las opciones que se le pase en el array de opciones
     */
    public function actualizarOpciones(ActualizarOpcionesRequest $request)
    {

        return response()->json(["data" => OpcionResource::collection($request->actualizarOpciones())]);
    }

    /**
     * Actualiza una opción dada por la ruta
     */
    public function actualizarOpcion( string $clave_opcion, ActualizarOpcionRequest $request)
    {
        if(OpcionFacade::esModificable($clave_opcion)) {
            // Actualizamos la opción
            OpcionFacade::set($clave_opcion, $request->input('valor'), $request->resolverModelo());
        }

        return response()->json(["data" => OpcionResource::make(OpcionFacade::getModelo($clave_opcion, $request->resolverModelo()))]);
    }

    public function obtenerModelos()
    {
        $modelos = collect(OpcionFacade::opciones())->map(fn($config)=>$config['relaciones'] ?? null);

        return response()->json(['data' => $modelos]);
    }
}
