<?php
namespace Beat\PaqueteOpcionesBeat\Http\Resources;

use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Beat\PaqueteOpcionesBeat\Models\Opcion $resource
 */
class OpcionResource extends JsonResource
{

    public function toArray($request):array
    {
        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        return [
            'clave'     => $this->resource->clave,
            'valor'     => $opciones_svc->getTyped($this->resource->clave, $this->resource->opcionable),
            'relacion'  => $this->when(!is_null($this->resource->opcionable), $this->resource->opcionable, null),
        ];
    }
    
}
