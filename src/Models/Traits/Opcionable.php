<?php

namespace Beat\PaqueteOpcionesBeat\Models\Traits;

use Beat\PaqueteOpcionesBeat\Models\Opcion;
use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property \Illuminate\Database\Eloquent\Collection<Opcion> $opciones
 */
trait Opcionable
{

    /**
     * Método que establece la relación entre opciones y el modelo
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    protected function opciones(): MorphMany
    {
        return $this->morphMany(Opcion::class, 'opcionable');
    }

    /**
     * Devuelve `true` si el modelo tiene opciones asociadas, 
     * false de lo contrario
     * @return bool
     */
    public function hasOpciones(): bool
    {
        return $this->opciones()->count() > 0;
    }

    /**
     * Crea o modifica una opción relacionada al modelo.
     * @param string $clave
     * @param mixed  $valor
     * @return array|float|int|mixed|string
     */
    public function setOpcion(string $clave, $valor)
    {

        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        $opciones_svc->set($clave, $valor, $this);

        return $opciones_svc->getTyped($clave, $this);
    }

    /**
     * Obtiene una opción relacionada al modelo
     * @param string $clave
     * @return array|float|int|mixed|string
     */
    public function getOpcion(string $clave)
    {

        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        return $opciones_svc->getTyped($clave, $this);
    }
}
