<?php

namespace Beat\PaqueteOpcionesBeat\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string      $clave
 * @property mixed       $valor
 * @property string      $opcionable_type
 * @property int         $opcionable_id
 * @property Model|null  $opcionable
 */
class Opcion extends Model
{
    protected $table = 'opciones';

    protected $fillable = [
        'clave',
        'valor',
        'opcionable_id',
        'opcionable_type',
    ];

    protected $casts = [
        'valor'      => 'string',
        'clave'      => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function opcionable()
    {
        return $this->morphTo('opcionable');
    }

    public static function boot()
    {
        parent::boot();

        // Registro de eventos del modelo
        foreach (config('beat_opciones.model_dispatches_events', []) as $key => $event) {

            self::$key(function (self $opcion) use ($event){
                $event::dispatch($opcion);
            });
        }
    }
}