<?php

use Beat\PaqueteOpcionesBeat\Events\OpcionModificadaEvent;
use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;

return [

    /*
    |--------------------------------------------------------------------------
    | Configuración general
    |--------------------------------------------------------------------------
    */

    // Middlewares que se aplicarán a las rutas
    'middlewares' => ['web'],

    // Canal donde aparecerán los logs
    'log_channel' => 'main',

    // Prefijo para las rutas
    'path' => 'api',

    // Eventos lanzados por el modelo Opcion
    'model_dispatches_events' => [
        'saved' => OpcionModificadaEvent::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Configuración de opciones
    |--------------------------------------------------------------------------
    */

    // Tag para guardar las opciones en Cache
    'cache_tags' => ['Opciones'],

    // valor por defecto de la opción si no se encuentra en cache
    'valor_cache_por_defecto' => 'UNDEFINED',

    // Array con las opciones disponibles
    'opciones' => [
        // Ejemplo de estructura
        // Nombre de la opción. Obligatorio 
        'OPCION_STRING' => [
            'modificable'   => false,
            // Tiempo de vida de la caché en segundos. Opcional.
            'cache'         => OpcionesService::SEGUNDOS_DIA,
            // Valor por defecto de la opción. Opcional.
            'default'       => '0.0.1',
            // Tipo de dato. Obligatorio
            'tipo'          => 'string',
            // Reglas de validación. Opcional
            // Puedes crear tus propias reglas personalizadas
            // o usar las que laravel ofrece
            'rules'         => ['data' => ['string', "regex:/^\d+\.\d+\.\d+$/"]],
            // Mensaje para errores de validación. Opcional
            'messages'      => ['regex' => 'Debe estar compuesto de 3 números separados por puntos ("1.0.0", por ejemplo).']
        ],
        'OPCION_ARRAY' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => ['valor1', 'valor2'],
            'tipo'      => 'array',
            'rules'     => ['data' => ['array', 'min:1'], 'data.*' => ['string']],
            'messages'  => ['data.*' => 'Los elementos deben ser una cadena de texto.']
        ],
        'OPCION_FLOAT' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => 13.99,
            'tipo'      => 'float',
            'rules'     => ['data' => ['numeric', 'min:1']],
        ],
        'OPCION_CON_RELACION' => [
            'cache'       => OpcionesService::SEGUNDOS_AÑO,
            'default'     => 'Con relacion',
            'tipo'        => 'string',
            'rules'       => ['data' => ['string']],
            'relaciones'  => [FakeUser::class]
        ],
        'OPCION_CON_POLITICA' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => 99.99,
            'tipo'      => 'float',
            'rules'     => ['data' => ['numeric', 'min:1']],
            'policy'    => ['clase' => FakeUser::class, 'metodo' => 'updateOpcion']
        ],
        'OPCION_BOOL' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => false,
            'tipo'      => 'bool',
            'rules'     => ['data' => ['boolean']],
        ],
        'OPCION_NULL' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => null,
            'tipo'      => 'string',
            'rules'     => ['data' => ['string','nullable']],
        ],
    ],

];
