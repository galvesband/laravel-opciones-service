<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opciones', function (Blueprint $table) {
            $table->after('valor', function (Blueprint $schema){
                $schema->nullableMorphs('opcionable');
            });

            $table->dropUnique('opciones_clave_unique');
            $table->unique(['clave', 'opcionable_type', 'opcionable_id'], 'clave_relacion_modelo_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opciones', function (Blueprint $table) {
            $table->dropMorphs('opcionable');
        });
    }
}
