# Configuración

Si el paquete está instalado, podemos seguir con el siguiente paso, la configuración.

Para comenzar necesitaremos publicar el archivo de configuración con el siguiente comando dentro de la carpeta de nuestro proyecto: `vendor:publish --tag=config`.

Con esto podremos encontrar el archivo de configuración `config/beat_opciones.php` en nuestro proyecto laravel. En cuanto a la configuración del paquete, podemos distinguir dos pilares fundamentales:
1. Las opciones disponibles. Sin ellas será inútil todo lo demás.

2. Configuración general del paquete. Configuraciones modificables para el uso del paquete.
Sin cambiar nada de esto, todo debe funcionar perfectamente. Sólo son para darle algo de personalización.

## Opciones



Cuándo publicamos el archivo de configuración nos encontraremos con que contiene una clave **opciones** en la que se recogen todas las opciones disponibles, y tiene esta estructura:

```php
      // Array con las opciones disponibles
      'opciones'                 => [
        // Ejemplo de estructura
        // Nombre de la opción. Obligatorio 
        'OPCION_STRING' => [
          // Tiempo de vida de la caché en segundos. Opcional.
          'cache' => OpcionesService::SEGUNDOS_DIA,
          // Valor por defecto de la opción. Opcional.
          'default' => '0.0.1',
          // Tipo de dato. Obligatorio
          'tipo' => 'string',
          // Reglas de validación. Opcional
          // Puedes crear tus propias reglas personalizadas
          // o usar las que laravel ofrece
          'rules' => ['data' => ['string', "regex:/^\d+\.\d+\.\d+$/"]],
          // Mensaje para errores de validación. Opcional
          'messages' => ['regex' => 'Debe estar compuesto de 3 números separados por puntos ("1.0.0", por ejemplo).'],
          'modificable' => false,
        ],
        'OPCION_ARRAY' => [
          'cache' => OpcionesService::SEGUNDOS_AÑO,
          'default' => ['valor1', 'valor2'],
          'tipo' => 'array',
          'rules' => ['data' => ['array', 'min:1'], 'data.*' => ['string']],
          'messages' => ['data.*' => 'Los elementos deben ser una cadena de texto.']
        ],
        'OPCION_FLOAT' => [
          'cache' => OpcionesService::SEGUNDOS_AÑO,
          'default' => 13.99,
          'tipo' => 'float',
          'rules' => ['data' => ['numeric', 'min:1']],
        ],
        'OPCION_CON_RELACION' => [
            'cache'       => OpcionesService::SEGUNDOS_AÑO,
            'default'     => 'Con relacion',
            'tipo'        => 'string',
            'rules'       => ['data' => ['string']],
            'relaciones'  => [FakeUser::class]
        ],
        'OPCION_CON_POLITICA' => [
            'cache'     => OpcionesService::SEGUNDOS_AÑO,
            'default'   => 99.99,
            'tipo'      => 'float',
            'rules'     => ['data' => ['numeric', 'min:1']],
            'policy'    => ['clase' => FakeUser::class, 'metodo' => 'updateOpcion']
        ]
      ],

```



Tenemos un mapa, cuya clave es la **clave de la opción** y el valor son los **campos que definen la naturaleza de la opción**.
Pasamos a detallar el significado de los campos:

1. **cache**: tiempo, en segundos, que la caché va a tener almacenada ésta opción.
Este campo es ***opcional***, si no se especifica, quedará guardado indefinidamente.
Debe ser un `int` (segundos) o un objeto `DateTime` (fecha en la que debe olvidar el valor).

2. **default**: valor que la opción tomará por defecto si el servicio no la encuentra en base de datos.
Este campo es ***obligatorio***. Debe ser del tipo que permita tu opción.

3. **tipo**: tipo de dato de tu opción. Este campo es ***obligatorio***,
ya que es usado para obtener el valor de la opción casteado a su tipo de dato original con el método `getType`.
Actualmente el paquete soporta los siguientes tipos:

    - string
    - integer
    - float
    - array
    - json
4. **rules**: [reglas de validación de laravel](https://laravel.com/docs/8.x/validation#available-validation-rules) para asegurarnos que nuestra opción siempre tomará un valor coherente.
Este campo es ***opcional***, si no se especifica, el valor a tomar por nuestra opción no será sometido a validaciones.
Debe ser un `array`.
5. **messages**: mensajes a mostrar cuando las reglas de validación fallan.
Este campo es ***opcional***, si no se especifica, los mensajes mostrados serán los que lanza [laravel por defecto](https://laravel.com/docs/8.x/validation#customizing-the-error-messages).
Debe ser un `array`.
6. **modificable**: si este campo está presente y su valor es `false`, la opción no podrá ser modificada a menos que sea mediante consola (`tinker`).
Este campo es **opcional** y de tipo `boolean`.
7. **relaciones**: es un `array` que contiene clases de modelos que podrán ser relacionados a esta opción. Es **opcional**, por lo que si no está presente, esa opción no podrá ser relacionada a modelos.
8. **policy**: es un `array` que contiene dos claves, _clase_ (clase del modelo relacionado con la política) y _metodo_ (método de la política donde se comprobará el acceso del usuario). Con esto, el paquete internamente controlará el acceso al endpoint `PUT opciones/{clave_opcion}` ejecutando la política que se desee. Puede ser muy útil si queremos proteger ciertas opciones de usuarios con ciertos permisos o carencia de ellos. Esta configuración es **opcional**.

En este array de configuración, podemos añadir tantas opciones cómo nuestro proyecto nos pida.
Cabe destacar que si intentamos obtener o actualizar una opción que no esté registrada en este archivo, el `OpcionesService` nos lanzará una excepción.

### Consejo

Personalmente aconsejo crear un modelo en tu proyecto y ahí poder declarar tus opciones para evitar __harcodear__ el nombre de estas.

```php
<?php

namespace App\Models;

use Beat\PaqueteOpcionesBeat\Models\Opcion as OpcionBeat;

class Opcion extends OpcionBeat
{
    /********************
     * CLAVES DE OPCIONES
     ********************/

    const EMAIL_DRIVER           = 'EMAIL_DRIVER';
    const EMAIL_PORT             = 'EMAIL_PORT';
    const EMAIL_HOST             = 'EMAIL_HOST';
    const EMAIL_USERNAME         = 'EMAIL_USERNAME';
    const EMAIL_PASSWORD         = 'EMAIL_PASSWORD';
    const EMAIL_ENCRYPTION       = 'EMAIL_ENCRYPTION';
    const EMAIL_FROM_ADDRESS     = 'EMAIL_FROM_ADDRESS';
    const RAZON_SOCIAL           = 'RAZON_SOCIAL';
    const DIRECCION_FACTURACION  = 'DIRECCION_FACTURACION';
    const CIF                    = 'CIF';
    const LOGO                   = 'LOGO';

    /******************
     * GRUPOS DE CLAVES
     ******************/

    const OPCIONES_EMAIL = [
        self::EMAIL_DRIVER,
        self::EMAIL_PORT,
        self::EMAIL_HOST,
        self::EMAIL_USERNAME,
        self::EMAIL_PASSWORD,
        self::EMAIL_ENCRYPTION,
        self::EMAIL_FROM_ADDRESS
    ];

    const OPCIONES_EMPRESA = [
        self::RAZON_SOCIAL,
        self::DIRECCION_FACTURACION,
        self::CIF,
        self::LOGO,
    ];
}
```

Una vez hayas hecho esto, podrás sustituir el nombre de las claves en el archivo de configuración.

```php
// Array con las opciones disponibles
      'opciones'                 => [
        Opcion::EMAIL_DRIVER => [
          'cache'   => SEGUNDOS_ANIO,
          'default' => 'smtp',
          'tipo'    => 'string',
          'rules'   => ['data' => ['string']],
        ],
        Opcion::EMAIL_PORT => [
            'cache'   => SEGUNDOS_ANIO,
            'default' => 25,
            'tipo'    => 'integer',
            'rules'   => ['data' => ['numeric']],
        ],
      ]
```

Así disminuimos la probabilidad de equivocarnos con el nombre de las opciones al llamarlas desde cualquier sitio de tu proyecto. 

## Configuración general



Estas configuraciones vienen definidas por defecto para que todo pueda funcionar correctamente, sólo hay que modificarlas si se quiere moldear más la lógica con tu proyecto.
La estructura es la siguiente:

```php

// Middlewares que se aplicarán a las rutas
'middlewares'              => ['web'],

// Prefijo para las rutas
'path'                     => 'api',

// Eventos lanzados por el modelo Opcion
'model_dispatches_events'  => [
                                  'saved' => OpcionModificadaEvent::class
                              ],

```
Con los comentarios se puede intuir la función de cada una de las configuraciones, aún así, pasamos a detallarlas:

1. **middlewares**: es un array con los middlewares que se aplicarán a las dos rutas que define el paquete.

2. **path**: es el prefijo que se añadirán a las rutas.

3. **model_dispatches_events**: esta es la más interesante. Esto es un mapeo con los eventos que se lanzarán
cuando se disparen los [triggers (eventos) de los modelos de laravel](https://laravel.com/docs/8.x/eloquent#events), estos pueden ser:
`retrieved`, `creating`, `created`, `updating`, `updated`, `saving`, `saved`, `deleting`, `deleted`, `restoring`, `restored`, y `replicating`.
Por defecto el paquete trae un evento que se lanza cuando una opción es modificada.
Siéntete libre de modificar esto a tu gusto, creando eventos personalizados para cada caso.

- [Uso](/docs/uso.md)