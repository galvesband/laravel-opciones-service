# Comandos disponibles

## Eliminar caché
Con el comando `php artisan beat-opciones:cache-clear` podemos eliminar la caché de una, varias o todas las opciones.
Admite un parámetro y dos opciones:
 - **opciones**: parámetro opcional múltiple para indicar qué opciones queremos eliminar de la caché. El valor es la clave de la opción objetivo. Ej: `php artisan beat-opciones:cache-clear OPCION_1 OPCION_2`.
 El ejemplo anterior eliminaría de la caché el valor de las opciones "OPCION_1" y "OPCION_2". Si no le pasamos ninguna clave, se eliminarán todas las opciones de la caché.
 
 - **--id_modelo**: opción para indicar la ID del modelo que está relacionado con la/s opcion/es a eliminar.

 - **--clase_modelo**: opción para indicar la clase del modelo que está relacionado con la/s opcion/es a eliminar.

 **IMPORTANTE** las opciones relacionadas con el modelo, (--id_modelo y --clase_modelo), deben proporcionarse juntas para poder resolver el modelo en cuestión. Si se indica una de ellas y no la otra, el comando lanzará un error y parará el proceso.
 
 ### Ejemplos de uso
- Eliminar una opción: `php artisan beat-opciones:cache-clear OPCION_1`

- Eliminar una opción relacionada a un modelo: `php artisan beat-opciones:cache-clear OPCION_1 --id_modelo=1 --clase_modelo="App/Models/User"`.

- Eliminar varias opciones: `php artisan beat-opciones:cache-clear OPCION_1 OPCION_2`

- Eliminar varias opciones de un modelo: `php artisan beat-opciones:cache-clear OPCION_1 OPCION_2 --id_modelo=1 --clase_modelo="App/Models/User"`

- Eliminar todas las opciones: `php artisan beat-opciones:cache-clear`