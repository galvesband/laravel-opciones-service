# Obtener/modificar opciones mediante el servicio
El servicio `opcionesService` tiene tres métodos fundamentales:
- `get(string $clave, Model|null $modelo): string`: devuelve el valor de la opción. Si recibe un modelo, buscará esa opción relacionada al modelo.
- `getTyped(string $clave, Model|null $modelo): mixed`: lo mismo que el anterior pero con el valor parseado al tipo especificado en la configuración de la opción.
- `set(string $clave, mixed $valor, Model|null $modelo): void`: crea o actualiza el valor de la opción. Si recibe un modelo, creará/modificará la opción relacionada.

**CONSEJO** también se puede hacer uso de la **fachada** que provee el paquete, `OpcionFacade`, para acceder a estos métodos, ej: `OpcionFacade::get($clave, $modelo)`. Si se prefiere un alias personalizado para la fachada, sólo debemos añadir esta línea al array "aliases" del archivo de configuración `config/app.php`:
`'Opciones'     => Beat\PaqueteOpcionesBeat\Facades\OpcionFacade::Class`

## Relaciones con modelos
Si queremos que una opción sea relacioada a modelos, cómo hemos visto anteriormente, en el archivo de configuración debemos añadir el campo "relaciones" a la configuración de la opción. Además de esto, podemos añadir el trait `Opcionable` a los modelos que van a ser relacionados.
Por detrás, el trait establece una relación polimórfica (`opcionable_id` y `opcionable_type`) entre el modelo y la tabla `opciones`. Además proporciona algunos métodos útiles para el manejo de la relación:

- `hasOpciones(): bool` : devuelve `true` si el modelo tiene opciones relacionadas, false de lo contrario.
- `setOpcion(string $clave_opcion, mixed $valor): void` : crea o actualiza una relación entre el modelo y la opción.
- `getopcion(string $clave): void` : obtiene la opción relacionada al modelo.