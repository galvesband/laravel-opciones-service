# Instalación



El paquete se encuentra disponible en [gitlab](https://gitlab.com/galvesband/laravel-opciones-service) como un paquete público.
para instalarlo, sólo debes introducir el comando `composer require beat/paquete-opciones-beat` seguido de `composer install` dentro de la carpeta de nuestro proyecto laravel.

## Integrar paquete-opciones-beat en un proyecto Laravel

El paquete está instalado, pero aún no está listo para ser utilizado o configurado. Debemos seguir los siguientes pasos:

### 1. Ejecutar las migraciones


Para poder usar el paquete debemos tener las migraciones ejecutadas, para así poder guardar nuestras opciones en base de datos.
El paquete está pensado para no tener que publicar las migraciones manualmente, sólo necesitarás ejecutar el
comando `php artisan migrate` para crear las tablas.
Después de esto ya debemos tener la **tabla opciones** en nuestra base de datos.

### 2. Publicar el archivo de configuración


Si queremos definir nuestras posibles opciones y moldear un poco el paquete a la forma de trabajar de nuestro proyecto, debemos publicar y configurar el archivo de configuración `config/beat_opciones.php`.
Para ello visite la [documentación](/docs/configuration.md).


