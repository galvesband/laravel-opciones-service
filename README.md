# Paquete Opciones Beat



La idea es que este paquete pueda manejar de forma muy simple el almacenamiento de opciones de una aplicación.
Un buen ejemplo de uso sería reemplazar constantes en el archivo `.env`, cómo configuraciones de correo o la dirección fiscal de una empresa,
por lo que ofrece este paquete y así hacer estas configuraciones dinámicas sin tener que estar tocando archivos delicados.

## ¿Cómo se gestionan las opciones?



Muy sencillo, a través del servicio `OpcionesService.php` puedes obtener (método `get` o `getTyped`) y modificar (método `set`) las opciones definidas en el archivo de configuración `beat_opciones.php` dentro de la clave **opciones**. Este servicio almacena las opciones en base de datos en la tabla `opciones`:
- **clave**: nombre de la opción. `string`
- **valor**: valor de la opción. `json`
- **opcionable_type**: clase del modelo relacionado, si lo tuviese. `string|null`
- **opcionable_id**: id del modelo relacionado, si lo tuviese. `int|null`.


Una vez el servicio crea o modifica la opción, ésta es guardada también en caché para así evitar llamadas repetitivas a la base de datos.
El paquete ofrece cinco rutas ya definidas con las que poder gestionar las opciones sin tener que añadir más código a tu proyecto:

- `GET => /opciones/{clave}`: recibe los parámetros "modelo_id" y "modelo_clase". Devuelve el valor de la opción encontrada según la clave recibida en la url.

- `GET => /opciones`: recibe un array con las claves de las opciones a listar y los parámetros "modelo_id" y "modelo_clase". Devuelve la siguiente estructura:
```php

[
    [
        clave       => clave1,
        valor       => valor1,
        relacion    => // objeto del modelo relacionado o null
    ],
    [
        clave       => clave2,
        valor       => valor2,
        relacion    => // objeto del modelo relacionado o null
    ],
]

```
- `PUT => /opciones`: recibe los siguientes parámetros:

```php

[
    "opciones"                  => "required|array",
    "opciones.*"                => "required|array",
    "opciones.*.clave"          => ["required",'string', Rule::in(array_keys(OpcionesService::OPCIONES()))],
    "opciones.*.valor"          => ["required"],
    "opciones.*.modelo_clase"   => ["sometimes", 'required_with:modelo_id', 'string'],
    "opciones.*.modelo_id"      => ["sometimes", 'required_with:modelo_clase', 'numeric'],
]

```
Actualiza cada opción recibida y devuelve la misma estructura que el endpoint anterior.
- `PUT => /opciones/{clave}`: modifica el valor de la opción encontrada según la clave recibida en la url. Recibe un parámetro "valor", "modelo_id" y "modelo_clase". Devuelve el valor de la opción actualizada.

- `GET => /opciones/modelos`: devuelve los modelos que pueden relacionarse a cada opción.

**IMPORTANTE**: los parámetros `modelo_clase` y `modelo_id` son opcionales en todos los endpoints.

Ver [guía de configuración](./docs/configuration.md#opciones) para saber cómo añadir opciones.

## Requisitos



- PHP (7.4, 8.0, 8.1 o 8.2)
- Laravel 8.x

## Guía de usuario

- [Instalación](/docs/installation.md)
- [Configuración](/docs/configuration.md)
- [Uso](/docs/uso.md)
- [Comandos](/docs/commands.md)