<?php

namespace Beat\PaqueteOpcionesBeat\Tests;

use Beat\PaqueteOpcionesBeat\BeatOpcionesServiceProvider;
use Beat\PaqueteOpcionesBeat\Tests\Misc\CreateFakeUsersTable;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestCase extends \Orchestra\Testbench\TestCase
{

    use RefreshDatabase;

    protected function getPackageProviders($app): array
    {
        return [
            BeatOpcionesServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app): void
    {
        include_once (__DIR__ . '/Misc/create_fake_users_table.php');
        // Migraciones fake
        $migracion = new CreateFakeUsersTable;

        $migracion->up();
    }
}
