<?php
namespace Beat\PaqueteOpcionesBeat\Tests\Feature;

use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;
use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUserFactory;
use Beat\PaqueteOpcionesBeat\Tests\TestCase;

class OpcionesServiceTest extends TestCase
{

    /**
     * Comprobamos que podemos obtener una opción através del endpoint
     */
    public function test_aceptacion_index()
    {
        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        $nuevo_valor = '0.1.1';

        //Modificamos una opción
        $opciones_svc->set('OPCION_STRING', $nuevo_valor);
        
        $params = [
            'opciones' => ['OPCION_STRING']
        ];

        $response = $this->json('GET','api/opciones', $params);

        $response->assertOk();
        $this->assertCount(1, $response->json('data'));
        $this->assertEquals($response->json('data.0.valor'), $nuevo_valor);
    }

    /**
     * Comprobamos que podemos obtener una opción através del endpoint filtrando por modelo
     */
    public function test_index_filtro_modelo()
    {
        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        $nuevo_valor = '0.1.1';

        $usuario = FakeUser::newFactory()->create();

        //Modificamos una opción para el usuario
        $opciones_svc->set('OPCION_CON_RELACION', $nuevo_valor, $usuario);

        //Modificamos una opción sin modelo
        $opciones_svc->set('OPCION_CON_RELACION', $nuevo_valor);
        
        $params = [
            'opciones'      => ['OPCION_CON_RELACION'],
            'modelo_clase'  => get_class($usuario),
            'modelo_id'     => $usuario->id,
        ];

        $response = $this->json('GET','api/opciones', $params);

        $response->assertOk();

        // Solo debe devolvernos una, la relacionada al modelo
        $this->assertCount(1, $response->json('data'));
        $this->assertEquals($response->json('data.0.valor'), $nuevo_valor);
        $this->assertEquals($response->json('data.0.clave'), 'OPCION_CON_RELACION');
        $this->assertEquals($response->json('data.0.relacion.name'), $usuario->name);
    }


    public function test_aceptacion_show()
    {
        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        $valor = '0.1.2';
        $opciones_svc->set('OPCION_STRING', $valor);

        $response = $this->json('GET', 'api/opciones/OPCION_STRING');
        $response->assertOk();
        $this->assertEquals($valor, $response->json('data.valor'));
    }

    public function test_aceptacion_show_con_relacion()
    {
        /** @var OpcionesService $opciones_svc */
        $opciones_svc = app(OpcionesService::class);

        $valor = '0.1.2';
        $opciones_svc->set('OPCION_CON_RELACION', 'otro valor');

        $usuario = FakeUser::newFactory()->create();

        //Modificamos una opción para el usuario
        $opciones_svc->set('OPCION_CON_RELACION', $valor, $usuario);

        $param = [
            'modelo_clase'  => get_class($usuario),
            'modelo_id'     => $usuario->id,
        ];

        $response = $this->json('GET', 'api/opciones/OPCION_CON_RELACION', $param);
        $response->assertOk();
        $this->assertEquals($valor, $response->json('data.valor'));
        $this->assertEquals($usuario->name, $response->json('data.relacion.name'));
    }

    public function test_devuelve_valor_por_defecto_si_opcion_no_esta_establecida()
    {
        $response = $this->json('GET', 'api/opciones/OPCION_STRING');
        $response->assertOk();
        $this->assertEquals(config('beat_opciones.opciones.OPCION_STRING.default'), $response->json('data.valor'));
    }

    /**
     * Comprobamos que podemos modificar una opción através del endpoint
     */
    public function test_aceptacion_put()
    {
        $nuevo_valor = '0.2.1';

        $params = [
            'opciones' => [

                [
                    'clave' => 'OPCION_STRING',
                    'valor' => $nuevo_valor
                ]
            ]
        ];

        $response = $this->json('PUT','api/opciones', $params);

        $response->assertOk();
        $this->assertCount(1, $response->json('data'));

        // Comprobamos que se ha actualizado correctamente
        $this->assertEquals($response->json('data.0.valor'), $nuevo_valor);
    }

    /**
     * Comprobamos que el endpoint devuelve un error de validación.
     */
    public function test_fallo_validacion()
    {
        $nuevo_valor = ['0.2.1'];

        $params = [
            'opciones' => [

                [
                    'clave' => 'OPCION_STRING',
                    'valor' => $nuevo_valor
                ]
            ]
        ];

        $response = $this->json('PUT','api/opciones', $params);

        $response->assertUnprocessable();
    }

    public function test_aceptacion_put_con_clave_en_url()
    {
        $nuevo_valor = '0.2.1';

        $usuario = FakeUser::newFactory()->create();

        $params = [
            'valor'         => $nuevo_valor,
            'modelo_clase'  => get_class($usuario),
            'modelo_id'     => $usuario->id,
        ];

        $response = $this->actingAs($usuario)->json('PUT','api/opciones/OPCION_CON_RELACION', $params);

        $response->assertOk();

        // Comprobamos que se ha actualizado correctamente
        $this->assertEquals($response->json('data.valor'), $nuevo_valor);
        $this->assertEquals($response->json('data.relacion.name'), $usuario->name);
    }

    public function test_aceptacion_put_con_clave_inexistente_en_url()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'RANDOM']);

        $nuevo_valor = '0.2.1';

        $params = ['valor' => $nuevo_valor];

        $response = $this->actingAs($usuario)->json('PUT','api/opciones/OPCION_NO_EXISTE', $params);

        $response->assertNotFound();
    }

    public function test_aceptacion_put_con_valor_incorrecto_en_url()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'RANDOM']);

        $nuevo_valor = ['0.2.1'];

        $params = ['valor' => $nuevo_valor];

        $response = $this->actingAs($usuario)->json('PUT','api/opciones/OPCION_STRING', $params);

        $response->assertUnprocessable();
    }

    public function test_aceptacion_modelos()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'RANDOM']);

        // Obtener modelos que pueden ser relacionados a cada opción
        $response = $this->actingAs($usuario)->json('GET','api/opciones/modelos');
        $response->assertOk();

        $this->assertCount(count(OpcionesService::opciones()), $response->json('data'));
        $this->assertCount(1, $response->json('data.OPCION_CON_RELACION'));

    }

    public function test_aceptacion_listado_claves()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'RANDOM']);
        $response = $this->actingAs($usuario)->json('GET','api/opciones/claves');
        $response->assertOk();

        $this->assertCount(count(OpcionesService::opciones()), $response->json('data'));
        $response->assertJsonStructure([
            'data' => [
                '*'=> [
                    'reglas',
                    'modificable',
                ]
            ]
        ]);
    }

    /**
     * Usuario llamado PAPA puede modificar opción
     * @return void
     */
    public function test_aceptacion_politicas()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'PAPA']);

        $clave = 'OPCION_CON_POLITICA';

        $params = [
            'valor'         => 100,
        ];

        $response = $this->actingAs($usuario)->json('PUT', "api/opciones/{$clave}", $params);
        $response->assertSuccessful();

        $this->assertDatabaseHas('opciones', 
        [
            'clave' => $clave,
            'valor' => 100
        ]);
    }

    /**
     * Usuario NO llamado PAPA NO puede modificar opción
     * @return void
     */
    public function test_politica_falla_sin_permisos()
    {
        $usuario = FakeUser::newFactory()->create(['name' => 'RANDOM']);

        $clave = 'OPCION_CON_POLITICA';

        $params = [
            'valor'         => 100,
        ];

        $response = $this->actingAs($usuario)->json('PUT', "api/opciones/{$clave}", $params);
        $response->assertForbidden();

        $this->assertDatabaseMissing('opciones', 
        [
            'clave' => $clave,
            'valor' => 99.99
        ]);
    }
}
