<?php

namespace Beat\PaqueteOpcionesBeat\Tests\Misc;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Beat\PaqueteOpcionesBeat\Models\Traits\Opcionable;

/**
 * @property int    $id
 * @property string $name
 * @property string $email
 */
class FakeUser extends Model implements AuthorizableContract, AuthenticatableContract
{
    use Authorizable, Authenticatable, HasFactory, Opcionable;

    protected $guarded = [];

    protected $table = 'users';

    public static function newFactory(): FakeUserFactory
    {
        return FakeUserFactory::new();
    }
}
