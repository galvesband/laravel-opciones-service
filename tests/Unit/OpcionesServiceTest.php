<?php
namespace Beat\PaqueteOpcionesBeat\Tests\Unit;

use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;
use Illuminate\Support\Facades\Event;
use Illuminate\Validation\ValidationException;
use Beat\PaqueteOpcionesBeat\Events\OpcionModificadaEvent;
use Beat\PaqueteOpcionesBeat\Services\OpcionesService;
use Beat\PaqueteOpcionesBeat\Tests\TestCase;

class OpcionesServiceTest extends TestCase
{
    protected OpcionesService $opciones_svc;

    protected function setUp():void
    {
        parent::setUp();

        $this->opciones_svc = app(OpcionesService::class);
    }

    /**
     * Comprobamos que el servicio funciona correctamente
     * puediendo modificar y obtener una opción
     */
    public function test_aceptacion()
    {
        $nuevo_valor = '0.1.1';

        //Modificamos una opción
        $this->opciones_svc->set('OPCION_STRING', $nuevo_valor);
        
        // Comprobamos que el valor se ha guardado correctamente
        $this->assertEquals($this->opciones_svc->get('OPCION_STRING'), $nuevo_valor);
        // Comprobamos también en BD
        $this->assertDatabaseHas('opciones', [
            'clave' => 'OPCION_STRING',
            'valor' => $nuevo_valor
        ]);
    }

    /**
     * Comprobamos que el servicio parsea correctamente 
     * el valor de las opciones. Comprobaremos los tipos string, array y float
     */
    public function test_parseo()
    {
        $nuevo_valor = '0.1.1';

        // PRUEBA TIPO STRING
        // Modificamos una opción
        $this->opciones_svc->set('OPCION_STRING', $nuevo_valor);

        $opcion_tipada = $this->opciones_svc->getTyped('OPCION_STRING');

        // Comprobamos que el valor se ha guardado correctamente
        $this->assertEquals($opcion_tipada, $nuevo_valor);

        // Comprobamos que es un string
        $this->assertIsString($opcion_tipada);

        // PRUEBA TIPO ARRAY
        $this->assertCount(2, $this->opciones_svc->getTyped('OPCION_ARRAY'));

        // Le añadimos un nuevo item al valor por defecto de la opción
        $this->opciones_svc->set('OPCION_ARRAY', ['valor1', 'valor2', 'valor3']);
        
        $this->assertDatabaseHas('opciones', [
            'clave' => 'OPCION_ARRAY',
            'valor' => json_encode( ['valor1', 'valor2', 'valor3'])
        ]);
        
        $opcion_tipada = $this->opciones_svc->getTyped('OPCION_ARRAY');
        
        // Comprobamos que sea de tipo array
        $this->assertIsArray($opcion_tipada);
        // Comprobamos que tenga un nuevo item
        $this->assertCount(3, $opcion_tipada);

        // PEUEBA TIPO FLOAT
        $nuevo_valor = 99.99;
        $this->opciones_svc->set('OPCION_FLOAT', $nuevo_valor);
        
        $opcion_tipada = $this->opciones_svc->getTyped('OPCION_FLOAT');
        $this->assertEquals($opcion_tipada, $nuevo_valor);
        $this->assertIsFloat($opcion_tipada);

        // PRUEBA TIPO BOOL
        $nuevo_valor = true;
        $this->opciones_svc->set('OPCION_BOOL', $nuevo_valor);
        
        $opcion_tipada = $this->opciones_svc->getTyped('OPCION_BOOL');
        $this->assertEquals($opcion_tipada, $nuevo_valor);
        $this->assertIsBool($opcion_tipada);
    }

    /**
     * Comprobamos que el servicio ejecuta bien las validaciones
     * sobre el valor a insertar.
     */
    public function test_validacion()
    {
        // Intentaremos darle un valor erróneo a una opción.
        // Debería lanzar un ValidationException
        $this->expectException(ValidationException::class);
        $this->opciones_svc->set('OPCION_ARRAY', 'valor_string');
    }

    public function test_evento_lanzado_al_modificar_opcion()
    {
        Event::fake(OpcionModificadaEvent::class);
        $this->opciones_svc->set('OPCION_STRING', '0.4.6');
        Event::assertDispatched(OpcionModificadaEvent::class);
    }

    public function test_relaciones_polimorficas_trait()
    {
        /** @var FakeUser $usuario */
        $usuario = FakeUser::newFactory()->create();

        // Comprobamos que el usuario no tiene ninguna opción asociada
        $this->assertFalse($usuario->hasOpciones());

        $usuario->setOpcion('OPCION_CON_RELACION', 'relacionado');
        
        // Creamos otra opcion para la misma clave pero sin modelo relacionado
        // para comprobar que no se sobreescriben
        $this->opciones_svc->set('OPCION_CON_RELACION', 'sin relacion');
        
        // Comprobamos que ahora el usuario si tiene opciones relacionadas
        $this->assertTrue($usuario->hasOpciones());

        $this->assertEquals('relacionado', $this->opciones_svc->get('OPCION_CON_RELACION', $usuario));
        $this->assertEquals('sin relacion', $this->opciones_svc->get('OPCION_CON_RELACION'));

    }

    public function test_relaciones_polimorficas_no_compatibles()
    {
        /** @var FakeUser $usuario */
        $usuario = FakeUser::newFactory()->create();

        // Intentamos asignar a un modelo una opción que no permite relaciones
        // Debe saltar excepción
        $this->expectException(\LogicException::class);
        $this->opciones_svc->set('OPCION_ARRAY', ['relacion no permitida'], $usuario);
    }

    /**
     * @test
     * Comprobaremos que una opción relacionada a un modelo
     * puede ser actualizada correctamente
     * @return void
     */
    public function test_modificar_relacion_polimorfica()
    {
        /** @var FakeUser $usuario */
        $usuario = FakeUser::newFactory()->create();

        // Le damos un primer valor
        $primer_valor = 'primer valor';
        $usuario->setOpcion('OPCION_CON_RELACION', $primer_valor);

        $this->assertEquals($primer_valor, $usuario->getOpcion('OPCION_CON_RELACION'));

        // Modificamos el valor
        $segundo_valor = 'segundo valor';
        $usuario->setOpcion('OPCION_CON_RELACION', $segundo_valor);

        $this->assertEquals($segundo_valor, $usuario->getOpcion('OPCION_CON_RELACION'));

        // Comprobamos base de datos
        // Sólo debe haber un registro
        $this->assertDatabaseCount('opciones', 1);
        // Los datos son los correctos
        $this->assertDatabaseHas('opciones', [
            'clave'             => 'OPCION_CON_RELACION',
            'valor'             => $segundo_valor,
            'opcionable_id'     => $usuario->id,
            'opcionable_type'   => get_class($usuario),
        ]);

    }

    public function test_valor_null()
    {
        $this->assertNull($this->opciones_svc->get('OPCION_NULL')); 
    }

}
