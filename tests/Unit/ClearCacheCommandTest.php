<?php

use Beat\PaqueteOpcionesBeat\Console\ClearCacheCommand;
use Beat\PaqueteOpcionesBeat\Facades\OpcionFacade;
use Beat\PaqueteOpcionesBeat\Tests\Misc\FakeUser;
use Beat\PaqueteOpcionesBeat\Tests\TestCase;

class ClearCacheCommandTest extends TestCase
{
    /**
     * Si no le pasamos una clave de opción, borra todas
     * @return void
     */
    public function test_todas_las_opciones()
    {

        $clave_opcion = 'OPCION_NULL';
        OpcionFacade::set($clave_opcion, 'nuevo valor');
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        
        $clave_opcion2 = 'OPCION_FLOAT';
        OpcionFacade::set($clave_opcion2, 13.3);
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion2)));


        $this->artisan(ClearCacheCommand::class)
        // Pregunta porque borrará todas las claves de la caché
        ->expectsConfirmation("Do you really wish to run this command?", 'yes')
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion2}'.")
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion}'.")
        ->assertExitCode(0);

        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion2)));
        
    }
    
    public function test_con_una_opcion_por_parametro()
    {
        $clave_opcion = 'OPCION_NULL';
        OpcionFacade::set($clave_opcion, 'nuevo valor');
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        
        $clave_opcion2 = 'OPCION_FLOAT';
        OpcionFacade::set($clave_opcion2, 13.3);

        $this->artisan(ClearCacheCommand::class,['opciones' => $clave_opcion])
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion}'.")
        // No se debe eliminar esta clave
        ->doesntExpectOutput("Se ha eliminado la cache de la opción '{$clave_opcion2}'.")
        ->assertExitCode(0);

        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        // NO ha borrado la cache de la que no le hemos pasado al comando
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion2)));

    }

    public function test_con_varias_opciones_por_parametro()
    {
        $clave_opcion = 'OPCION_NULL';
        OpcionFacade::set($clave_opcion, 'nuevo valor');
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        
        $clave_opcion2 = 'OPCION_FLOAT';
        OpcionFacade::set($clave_opcion2, 13.3);
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion2)));

        $this->artisan(ClearCacheCommand::class,['opciones' => [$clave_opcion, $clave_opcion2]])
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion}'.")
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion2}'.")
        ->assertExitCode(0);

        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion2)));

    }

    public function test_sin_confirmacion()
    {
        $clave_opcion = 'OPCION_NULL';
        OpcionFacade::set($clave_opcion, 'nuevo valor');
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
        
        $this->artisan(ClearCacheCommand::class)
        ->expectsConfirmation("Do you really wish to run this command?")
        ->assertExitCode(1)
        ->doesntExpectOutput("Se ha eliminado la cache de la opción '{$clave_opcion}'.");

        // NO la ha borrado
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion)));
    }

    public function test_con_modelo()
    {
        $user = FakeUser::newFactory()->create();

        $clave_opcion = 'OPCION_CON_RELACION';
        OpcionFacade::set($clave_opcion, 'nuevo valor', $user);
        $this->assertTrue($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion, $user)));
        
        
        $this->artisan(ClearCacheCommand::class, ['--id_modelo' => $user->id, '--clase_modelo' => FakeUser::class])
        ->expectsConfirmation("Do you really wish to run this command?", 'yes')
        ->expectsOutput("Se ha eliminado la cache de la opción '{$clave_opcion}'.")
        ->assertExitCode(0);

        $this->assertFalse($this->check_cache(OpcionFacade::resolverKeyCache($clave_opcion, $user)));
    }

    private function check_cache($key)
    {
        return Cache::tags(config('beat_opciones.cache_tags'))->offsetExists($key);
    }
}
